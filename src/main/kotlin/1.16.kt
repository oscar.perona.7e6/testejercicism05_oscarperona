/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.16 Transforma l’enter
*/

import java.util.*

fun main () {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val number: Int = scanner.nextInt()

    println(transformInt(number))

}
fun transformInt(number: Int):Double{
    return number.toDouble()
}