/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.15 Afegeix un segon
*/

import java.util.*

fun main () {

    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix el comensals y el preu:")
    val seconds: Int = scanner.nextInt()


    println(addSecond(seconds))
}
fun addSecond(seconds:Int):Int{
    return (seconds+1) % 60
}