import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.9 Calcula el descompte
*/


fun main(){

    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix el preu original:")
    val price: Double = scanner.nextDouble()
    println("Introdueix el preu amb descompte:")
    val finalPrice: Double = scanner.nextDouble()

    println(descompte(price, finalPrice))
}
fun descompte(price:Double, finalPrice:Double):Double{
    val percent: Double = ((finalPrice*100) / price)
    val discount: Double = 100- percent
    return discount
}