/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.13 Quina temperatura fa?
*/

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix la temperatura i el seu augment:")

    val temp: Double = scanner.nextDouble()
    val aug: Double = scanner.nextDouble()

    println(temperature(temp, aug))

}
fun temperature(temp:Double, aug:Double):Double{
    return temp + aug
}