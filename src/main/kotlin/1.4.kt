import java.util.*

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix amplada y llargada:")
    // Reads an int
    val amplada = scanner.nextInt()
    val llargada = scanner.nextInt()

    println(calculArea(amplada, llargada))
}

fun calculArea(amplada : Int, llargada : Int):Int{
    return amplada*llargada
}