import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.8 Dobla el decimal
*/

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número decimal:")

    val number: Double = scanner.nextDouble()
    println(dobleDecimal(number))
}
fun dobleDecimal(number: Double):Double{
    return number*2
}