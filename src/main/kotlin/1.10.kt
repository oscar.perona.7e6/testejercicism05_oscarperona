/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.10 Quina és la mida de la meva pizza?
*/

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix el diametre de la pizza:")

    val diametre: Double = scanner.nextDouble()

   println(pizzaArea(diametre))
}
fun pizzaArea(diametre:Double):Double{
    return (Math.PI/4)*(diametre*diametre)
}

