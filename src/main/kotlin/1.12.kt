/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.12 De Celsius a Fahrenheit
*/

import java.util.*

fun main () {

    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix la temperatura:")

    val celcius: Double = scanner.nextDouble()


    println(celsiusToFahrenheit(celcius))
}
fun celsiusToFahrenheit(celcius:Double):Double{
    return (celcius * 1.8) + 32
}