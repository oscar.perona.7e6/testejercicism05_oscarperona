/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.7 Número següent
*/


import java.util.*


fun main() {

    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val number: Int = scanner.nextInt()
    println(nextNumber(number))

}
fun nextNumber(number:Int):Int{
    return number+1
}