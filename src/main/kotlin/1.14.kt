/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.14 Divisor de compte
*/

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix el comensals y el preu:")

    val price: Double = scanner.nextDouble()
    val clients: Int = scanner.nextInt()


    println(divisorCompte(price, clients))
}
fun divisorCompte(price:Double, clients:Int):Double{
    return price / clients
}