import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_9KtTest{

    // Original price: 49.99 Final price: 35.30 Discount: 29.3858771754351
    // Original price: 49.99 Final price: 49.99 Discount: 0.0
    // Original price: 2.50 Final price: 2.00 Discount: 20.0

    @Test
    fun decimalDiscount(){
        assertEquals(29.3858771754351, descompte(49.99, 35.30))
    }

    @Test
    fun noDiscount(){
        assertEquals(0.0, descompte(49.99, 49.99))
    }

    @Test
    fun normalDiscount(){
        assertEquals(20.0, descompte(2.50, 2.00))
    }
}
