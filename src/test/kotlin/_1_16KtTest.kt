import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_16KtTest{

    // -5 --> -5.0
    // 2 --> 2.0
    // 99 --> 99.0

    @Test
    fun negativeNumber(){
        assertEquals(-5.0, transformInt(-5))
    }

    @Test
    fun lowNumber(){
        assertEquals(2.0, transformInt(2))
    }

    @Test
    fun highNumber(){
        assertEquals(99.0, transformInt(99))
    }
}