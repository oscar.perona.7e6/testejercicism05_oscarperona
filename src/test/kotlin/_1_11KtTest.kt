import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_11KtTest{

    // 5.50*5.50*5.50 = 166.375
    // 1.0*20.50*1.5 = 30.75
    // 20.80*60.7*33.33 = 42081.124800000005

    @Test
    fun equalNUmbers(){
        assertEquals(166.375, volumAire(5.50, 5.50, 5.50))
    }

    @Test
    fun oneHighNumber(){
        assertEquals(30.75, volumAire(1.0, 20.50, 1.5))
    }

    @Test
    fun highNumbers(){
        assertEquals(42081.124800000005, volumAire(20.80, 60.7, 33.33))
    }
}