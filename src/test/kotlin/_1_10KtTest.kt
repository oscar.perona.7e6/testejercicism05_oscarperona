import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_10KtTest{

    // 20.0 --> 314.1592653589793
    // 5.50 --> 23.75829444277281
    // 1.0  --> 0.7853981633974483

    @Test
    fun highDiametre(){
        assertEquals(314.1592653589793, pizzaArea(20.0))
    }

    @Test
    fun decimalDiametre(){
        assertEquals(23.75829444277281, pizzaArea(5.50))
    }

    @Test
    fun lowºDiametre(){
        assertEquals(0.7853981633974483, pizzaArea(1.0))
    }
}