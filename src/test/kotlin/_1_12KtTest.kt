import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_12KtTest{

    // -8.0 --> 17.6
    // 45.0 --> 113.0
    // 16.70 --> 60.8

    @Test
    fun negativeCelcius(){
        assertEquals(17.6, celsiusToFahrenheit(-8.0))
    }

    @Test
    fun highCelcius(){
        assertEquals(113.0, celsiusToFahrenheit(45.0))
    }

    @Test
    fun normalCelcius(){
        assertEquals(62.06, celsiusToFahrenheit(16.70))
    }
}