import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_8KtTest{

    // 5.5 *2 = 11.0
    // -2.2 = -4.4
    // 2.8579519 *2 = 5.7159038

    @Test
    fun decimanNumber(){
        assertEquals(11.0, dobleDecimal(5.5))
    }

    @Test
    fun negativeNumber(){
        assertEquals(-4.4, dobleDecimal(-2.2))
    }

    @Test
    fun highDecimalsNumber(){
        assertEquals(5.7159038, dobleDecimal(2.8579519))
    }
}