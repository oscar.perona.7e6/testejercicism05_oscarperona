import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_13KtTest{

    // 20.50 + 2.00 = 22.5
    // -3.0 + -1.50 = -4.5
    // 3.50 + 30.78 = 34.28

    @Test
    fun hightTemperature(){
        assertEquals(22.5, temperature(20.50, 2.00))
    }

    @Test
    fun negativeTemperature(){
        assertEquals(-4.5, temperature(-3.0, -1.50))
    }

    @Test
    fun lowTemperature(){
        assertEquals(34.28, temperature(3.50, 30.78))
    }
}