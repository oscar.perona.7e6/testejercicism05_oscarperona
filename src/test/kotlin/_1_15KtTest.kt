import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_15KtTest{

    // 0 --> 1
    // 30 --> 31
    // 59 --> 0

    @Test
    fun noSeconds(){
        assertEquals(1, addSecond(0))
    }

    @Test
    fun normalSeconds(){
        assertEquals(31, addSecond(30))
    }

    @Test
    fun finalSecond(){
        assertEquals(0, addSecond(59))
    }
}