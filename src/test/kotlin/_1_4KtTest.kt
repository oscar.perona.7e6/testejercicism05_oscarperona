import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_4KtTes{

    @Test
    fun bigNumber (){
        assertEquals(4000,calculArea(40,100))
    }
    @Test
    fun lowNumber (){
        assertEquals(4, calculArea(2, 2))
    }
    @Test
    fun positiveNumbers (){
        assertEquals(120, calculArea(15, 8))
    }
}