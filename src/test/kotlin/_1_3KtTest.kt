import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_3KtTest{

    // -6 + 7 = 1
    // -12 + -5 = -17
    // 4 + 6 = 10

    @Test
    fun negativeNumberPlusPositiveNumber(){
        assertEquals(1, operation(-6,7))
    }
    @Test
    fun negativeNumberSummary(){
        assertEquals(-17, operation(-12,-5))
    }
    @Test
    fun postiveNumberSummary(){
        assertEquals(10, operation(4, 6))
    }
}