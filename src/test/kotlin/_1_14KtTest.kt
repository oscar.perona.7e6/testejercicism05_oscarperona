import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_14KtTest{

    // 70.50 / 2 = 35.25
    // 20 / 5 = 4
    // 33.50 / 1 = 33.50

    @Test
    fun highPrice(){
        assertEquals(35.25, divisorCompte(70.50, 2))
    }

    @Test
    fun highPersons(){
        assertEquals(4.0, divisorCompte(20.0, 5))
    }

    @Test
    fun onePerson(){
        assertEquals(33.50, divisorCompte(33.50, 1))
    }
}