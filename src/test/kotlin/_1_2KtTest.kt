import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_2KtTest{

    //2*2 = 4
    //-5*2 = -10
    //25*2 = 50

    @Test
    fun chekIfOperationIsFour(){
        val expected = 4
        assertEquals(expected, operation(2))
    }

    @Test
    fun checkOperationWithNegativeNUmber(){
        val expected = -10
        assertEquals(expected, operation(-5))
    }

    @Test
    fun chekOperationWithHIghNUmber(){
        val expected = 50
        assertEquals(expected, operation(25))
    }
}


