import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_6KtTest{

    @Test
    fun numbersWith0(){
        assertEquals(27, pupitres(0,33,20))
    }
    @Test
    fun positiveNumbers(){
        assertEquals(3, pupitres(1,3,1))
    }
    @Test
    fun bigNumbers(){
        assertEquals(119, pupitres(100,80,58))
    }
}