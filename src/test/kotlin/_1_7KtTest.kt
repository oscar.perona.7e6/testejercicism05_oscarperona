import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_7KtTest{


    //150 --> 151
    //-8 --> -7
    //5 --> 6

    @Test
    fun negativeNumber(){
        assertEquals(151, nextNumber(150))
    }
    @Test
    fun positiveBigNumber(){
        assertEquals(-7, nextNumber(-8))
    }
    @Test
    fun checkZeroResult(){
        assertEquals(6, nextNumber(5))
    }

}