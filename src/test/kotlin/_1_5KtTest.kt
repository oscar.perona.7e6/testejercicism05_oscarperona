import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_5KtTest{
    @Test
    fun negativeNumber(){
        assertEquals(0, operation(8,-8,-1,1))
    }
    @Test
    fun allNegativeNumber(){
        assertEquals(0, operation(-8,-2,-10,-5))
    }
    @Test
    fun positiveNumbers(){
        assertEquals(30, operation(8,7,2,6))
    }
}